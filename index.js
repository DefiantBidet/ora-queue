const ora = require('ora');
const Queue = require('./queue');

recursiveRunner(context) {
  const queue = context.queue;
  const intervalId = context.intervalId;
  const ora = context.ora;

  if (queue.isEmpty()) {
    clearInterval(intervalId);
    context.stop();
    return;
  }
  const info = queue.dequeue();
  if (info.color) {
    ora.color = info.color;
  }

  if (info.text) {
    ora.text = info.text;
  }

  if (info.spinner) {
    ora.spinner = info.spinner;
  }
}

class OraMessageQueue {
  constructor(duration = 1250, oraOptions = {}) {
    this.duration = duration;
    this.oraOptions = oraOptions;
    this.queue = new Queue();
    this.ora = ora(this.oraOptions);
    this.intervalId = null;
  }

  /**
   * Adds a <OraQueueObject> to the queue.
   * <OraQueueObject> is an object with optional
   * `text`, `color`, `spinner` members
   * @param {Object} info ora settings to enqueue
   */
  add(info) {
    const oraData = {};
    if (info.color) {
      oraData.color = info.color;
    }

    if (info.text) {
      oraData.text = info.text;
    }

    this.queue.enqueue(oraData);
  }

  start() {
    const ora = this.ora;
    const queue = this.queue;
    const duration = this.duration;

    // nothing to start if queue is empty
    if (queue.isEmpty()) {
      return;
    }
    // don't retrigger setInterval if already running one.
    if (this.intervalId) {
      return;
    }

    const info = queue.dequeue();
    ora.start();
    if (info.color) {
      ora.color = info.color;
    }

    if (info.text) {
      ora.text = info.text;
    }

    if (info.spinner) {
      ora.spinner = info.spinner;
    }

    this.intervalId = setInterval(recursiveRunner, duration, this);

    return this;
  }



  /*********************************************************************
   * NOTE:
   *   The following methods are effectively wrappers to ora's methods.
   *   However, it is worth noting that prior to invoking the ora method,
   *   the wrappers clear the Queue. This causes the ora methods to be
   *   invoked instantaneously, as the queue is flushed.
   *
   ********************************************************************/

  /**
   * Stop and clear the spinner.
   * @return {OraMessageQueue}  The Queue instance for chaining
   */
  stop() {
    this.queue.clear();
    this.ora.stop();
    return this;
  }

  /**
   * Stop the spinner, change it to a green ✔ and persist the current
   * text, or text if provided.
   * @param  {String} text        Optional text to persist with ✔
   * @return {OraMessageQueue}    The Queue instance for chaining
   */
  succeed(text) {
    this.queue.clear();
    this.ora.succeed(text);
    return this;
  }

  /**
   * Stop the spinner, change it to a red ✖ and persist the current
   * text, or text if provided.
   * @param  {String} text        Optional text to persist with ✖
   * @return {OraMessageQueue}    The Queue instance for chaining
   */
  fail(text) {
    this.queue.clear();
    this.ora.fail(text);
    return this;
  }

  /**
   * Stop the spinner, change it to a yellow ⚠ and persist the current
   * text, or text if provided.
   * @param  {String} text        Optional text to persist with ⚠
   * @return {OraMessageQueue}    The Queue instance for chaining
   */
  warn(text) {
    this.queue.clear();
    this.ora.warn(text);
    return this;
  }

  /**
   * Stop the spinner, change it to a blue ℹ and persist the current
   * text, or text if provided.
   * @param  {String} text        Optional text to persist with ℹ
   * @return {OraMessageQueue}    The Queue instance for chaining
   */
  info(text) {
    this.queue.clear();
    this.ora.info(text);
    return this;
  }

  /**
   * Stop the spinner and change the symbol or text.
   * @param  {Symbol|String} options The Symbol or text to persist with
   * @return {OraMessageQueue}    The Queue instance for chaining
   */
  stopAndPersist(options) {
    this.queue.clear();
    this.ora.stopAndPersist(options);
    return this;
  }

  /**
   * Clear the spinner.
   * @return {OraMessageQueue}  The Queue instance for chaining
   */
  clear() {
    this.queue.clear();
    this.ora.clear();
    return this;
  }
}

module.exports = OraMessageQueue;
