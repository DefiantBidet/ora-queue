import test from 'ava';
import sinon from 'sinon';
import ora from 'ora';
import oraQueue from '.';
import Queue from './queue';

const noop = () => { /* noop */ };
const defaultDuration = 1250;

const WRITE_OVERRIDE = process.stderr.write;

test.before(() => {
  // silence ora output
  process.stderr.write = sinon.spy();
});

test.after(() => {
  process.stderr.write = WRITE_OVERRIDE;
});

test('oraQueue::constructor() returns an oraQueue', (t) => {
  const q = new oraQueue();
  t.true(q instanceof oraQueue);
});

test('oraQueue::constructor() initializes \'duration\' instance variable', (t) => {
  const q = new oraQueue();
  t.is(q.duration, defaultDuration);
});

test('oraQueue::constructor() initializes \'oraOptions\' instance variable', (t) => {
  const q = new oraQueue();
  t.deepEqual(q.oraOptions, {});
});

test('oraQueue::constructor() initializes \'queue\' instance variable', (t) => {
  const q = new oraQueue();
  t.true(q.queue instanceof Queue);
  t.is(q.queue.size, 0);
});

test('oraQueue::constructor() initializes \'ora\' instance variable', (t) => {
  const q = new oraQueue();
  t.truthy(q.ora);
});

test('oraQueue::constructor() initializes \'intervalId\' instance variable', (t) => {
  const q = new oraQueue();
  t.is(q.intervalId, null);
});

test.todo('oraQueue::methodName()');
